#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QProcess>
#include <QTemporaryFile>
#include <QFileInfo>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    convert(nullptr),
    logfile(new QTemporaryFile("qimg2pdf_XXXXXX.log", this)),
    viewWindow(new imageview(this))
{
    ui->setupUi(this);
    logfile->setAutoRemove(true);

    connect(ui->addButton, &QPushButton::clicked,
            this, &MainWindow::addImage);
    connect(ui->removeButton, &QPushButton::clicked,
            this, &MainWindow::removeImage);
    connect(ui->shiftUpButton, &QPushButton::clicked,
            this, &MainWindow::shiftImageUp);
    connect(ui->shiftDownButton, &QPushButton::clicked,
            this, &MainWindow::shiftImageDown);
    connect(ui->previewButton, &QPushButton::clicked,
            this, &MainWindow::displayPreview);
    connect(ui->generateButton, &QPushButton::clicked,
            this, &MainWindow::generate);
    connect(ui->fileButton, &QPushButton::clicked,
            this, &MainWindow::openSaveDialog);
    connect(ui->imageList, &QListWidget::currentItemChanged,
            this, &MainWindow::currentItemChanged);

    connect(viewWindow, &imageview::nextImage,
            this, &MainWindow::nextImage);
    connect(viewWindow, &imageview::previousImage,
            this, &MainWindow::previousImage);
    connect(this, &MainWindow::refreshPreview,
            viewWindow, &imageview::refreshImage);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addImage()
{
    QString initialDir;

    if (ui->imageList->count() > 0 && ui->imageList->currentItem() != nullptr)
    {
        QFileInfo currentPath(ui->imageList->currentItem()->text());
        initialDir = currentPath.dir().absolutePath();
    }
    else
    {
        initialDir = QDir::homePath();
    }
    QStringList paths = QFileDialog::getOpenFileNames(this,
                                                      tr("Vyberte obrázek ke vložení"),
                                                      initialDir,
                                                      tr("Obrázky (*.png *.jpg *.jpeg *.gif *.tif);; Všechny soubory (*.*)"),
                                                      nullptr);
    if (paths.size() == 0)
        return;

    int insertAt;
    QModelIndexList selected = ui->imageList->selectionModel()->selectedIndexes();
    if (selected.size() == 0) {
        insertAt = ui->imageList->count();
    } else {
        insertAt = 0;
        for (QModelIndex idx : selected) {
            insertAt = std::max(insertAt, idx.row());
        }
        insertAt++;
    }

    ui->imageList->insertItems(insertAt, paths);
    ui->imageList->setCurrentRow(insertAt + paths.count() - 1,
                                 QItemSelectionModel::ClearAndSelect
                                 | QItemSelectionModel::Current);
}

void MainWindow::removeImage()
{
    QList<QListWidgetItem*> items = ui->imageList->selectedItems();
    for (QListWidgetItem *item : items) {
        delete ui->imageList->takeItem(ui->imageList->row(item));
    }
}

void MainWindow::shiftImageUp()
{
    int currentRow = ui->imageList->currentRow();
    if (currentRow == 0)
        return;

    QListWidgetItem *currentItem = ui->imageList->takeItem(currentRow);
    ui->imageList->insertItem(currentRow - 1, currentItem);
    ui->imageList->setCurrentItem(currentItem,
                                  QItemSelectionModel::ClearAndSelect
                                  | QItemSelectionModel::Current);
}

void MainWindow::shiftImageDown()
{
    int currentRow = ui->imageList->currentRow();
    if (currentRow == ui->imageList->count() - 1)
        return;

    QListWidgetItem *currentItem = ui->imageList->takeItem(currentRow);
    ui->imageList->insertItem(currentRow + 1, currentItem);
    ui->imageList->setCurrentItem(currentItem,
                                  QItemSelectionModel::ClearAndSelect
                                  | QItemSelectionModel::Current);
}

void MainWindow::displayPreview()
{
    viewWindow->setGeometry(this->x() + this->width(),
                            this->y(),
                            this->width(),
                            this->height());
    viewWindow->setMinimumSize(this->size());
    viewWindow->setMaximumSize(this->size());
    viewWindow->resize(this->size());
    viewWindow->show();
}

void MainWindow::generate()
{
    if (convert != nullptr)
    {
        QMessageBox::warning(this,
                             tr("Probíhá převod"),
                             tr("Minulý převod ještě neskončil, prosím vyčkejte."),
                             QMessageBox::Ok);
        return;

    }
    if (ui->fileEntry->text().isEmpty())
    {
        openSaveDialog();
        if (ui->fileEntry->text().isEmpty()) {
            QMessageBox::warning(this,
                                 tr("Nebyl vybrán cíl"),
                                 tr("Vyberte prosím, kam uložit výsledné PDF."),
                                 QMessageBox::Ok);
            return;
        }
    }
    int dpi = ui->dpiEntry->value();
    QStringList cmdline;
    cmdline << "-o";
    cmdline << ui->fileEntry->text();
    cmdline << QString("-s %1dpix%1dpi").arg(dpi);
    for (int i = 0; i < ui->imageList->count(); i++) {
        QListWidgetItem *item = ui->imageList->item(i);
        cmdline << item->text();
    }

    if (!logfile->isOpen())
    {
        logfile->open();
    }

    convert = new QProcess(this);
    connect(convert, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
            this, &MainWindow::convertFinished);

    convert->setProgram("img2pdf");
    convert->setArguments(cmdline);
    convert->setInputChannelMode(QProcess::ManagedInputChannel);
    convert->setProcessChannelMode(QProcess::MergedChannels);
    convert->setStandardOutputFile(logfile->fileName());
    convert->setWorkingDirectory(QDir::tempPath());
    convert->start();

    ui->generateButton->setText(tr("Probíhá..."));
    ui->generateButton->setEnabled(false);
}

void MainWindow::openSaveDialog()
{
    QString oldPath = ui->fileEntry->text();
    if (oldPath.isEmpty()) {
        oldPath = QDir::homePath();
    }
    QString newPath = QFileDialog::getSaveFileName(this,
                                                   tr("Vyberte, kam uložit výsledné PDF"),
                                                   oldPath,
                                                   tr("Portable Document Format (*.pdf);; Všechny soubory (*.*)"),
                                                   nullptr);
    if (newPath.isEmpty())
        return;

    ui->fileEntry->setText(newPath);
}

void MainWindow::convertFinished(int exitCode, QProcess::ExitStatus)
{
    ui->generateButton->setText(tr("&Uložit PDF"));
    ui->generateButton->setEnabled(true);
    if (exitCode == 0)
    {
        QMessageBox::information(this,
                                 tr("Převod uspěl"),
                                 tr("PDF bylo úspěšně vytvořeno."),
                                 QMessageBox::Ok);
    }
    else
    {
        QMessageBox::warning(this,
                             tr("Převod selhal"),
                             tr("Vytvoření PDF selhalo. Více informací lze nalézt v souboru %1")
                             .arg(logfile->fileName()),
                             QMessageBox::Ok);
        logfile->setAutoRemove(false);
    }
    delete convert;
    convert = nullptr;
}

void MainWindow::nextImage()
{
    int current = ui->imageList->currentRow();
    int next;
    if (current == ui->imageList->count() - 1) {
        next = 0;
    } else {
        next = current + 1;
    }
    ui->imageList->setCurrentRow(next,
                                 QItemSelectionModel::ClearAndSelect
                                 | QItemSelectionModel::Current);
}

void MainWindow::previousImage()
{
    int current = ui->imageList->currentRow();
    int next;
    if (current == 0) {
        next = ui->imageList->count() - 1;
    } else {
        next = current - 1;
    }
    ui->imageList->setCurrentRow(next,
                                 QItemSelectionModel::ClearAndSelect
                                 | QItemSelectionModel::Current);
}

void MainWindow::currentItemChanged(QListWidgetItem *current, QListWidgetItem *)
{
    if (current == nullptr || current->text().isEmpty())
        return;

    emit refreshPreview(current->text());
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Escape:
        this->close();
        break;
    default:
        return QMainWindow::keyPressEvent(event);
    }
}
