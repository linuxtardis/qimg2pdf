#include "imageview.h"
#include "ui_imageview.h"
#include <QPixmap>
#include <QPixmapCache>

imageview::imageview(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::imageview)
{
    ui->setupUi(this);
    this->setModal(false);
    this->setWindowFlag(Qt::WindowStaysOnTopHint, false);
    this->setWindowFlag(Qt::WindowStaysOnBottomHint, false);
}

imageview::~imageview()
{
    delete ui;
}

void imageview::refreshImage(const QString &path)
{
    QPixmap *cached = QPixmapCache::find(path);
    if (cached) {
        ui->imageLabel->setPixmap(*cached);
    } else {
        QPixmap pix;
        if (pix.load(path)) {
            pix = pix.scaled(size(), Qt::KeepAspectRatio);
        }
        QPixmapCache::insert(path, pix);
        ui->imageLabel->setPixmap(pix);
    }
}

void imageview::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Left:
    case Qt::Key_Up:
        emit previousImage();
        break;
    case Qt::Key_Right:
    case Qt::Key_Down:
        emit nextImage();
        break;
    default:
        return QDialog::keyPressEvent(event);
    }
}
