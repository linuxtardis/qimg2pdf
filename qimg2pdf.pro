# Qt-based Img2Pdf

QT += core gui widgets

TARGET = qimg2pdf
TEMPLATE = app
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += \
        include

SOURCES += \
        src/main.cpp \
        src/mainwindow.cpp \
        src/imageview.cpp

HEADERS += \
        include/mainwindow.h \
        include/imageview.h

FORMS += \
        ui/mainwindow.ui \
        ui/imageview.ui

RESOURCES += \
        res/resources.qrc

RC_ICONS = res/pdf.ico
