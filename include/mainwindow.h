#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <imageview.h>
#include <QMainWindow>
#include <QProcess>
#include <QTemporaryFile>
#include <QKeyEvent>
#include <QListWidgetItem>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void addImage();
    void removeImage();
    void shiftImageUp();
    void shiftImageDown();
    void displayPreview();
    void generate();
    void openSaveDialog();

    void convertFinished(int exitCode, QProcess::ExitStatus status);

    void nextImage();
    void previousImage();

    void currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

signals:
    void refreshPreview(const QString &path);

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    Ui::MainWindow *ui;
    QProcess *convert;
    QTemporaryFile *logfile;
    imageview *viewWindow;
};

#endif // MAINWINDOW_H
