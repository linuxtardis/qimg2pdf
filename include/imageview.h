#ifndef IMAGEVIEW_H
#define IMAGEVIEW_H

#include <QKeyEvent>
#include <QDialog>

namespace Ui {
class imageview;
}

class imageview : public QDialog
{
    Q_OBJECT

public:
    explicit imageview(QWidget *parent = 0);
    ~imageview();

signals:
    void nextImage();
    void previousImage();

public slots:
    void refreshImage(const QString &path);

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    Ui::imageview *ui;
};

#endif // IMAGEVIEW_H
