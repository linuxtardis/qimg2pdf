cmake_minimum_required(VERSION 3.10)

include(GNUInstallDirs)

project("qimg2pdf" VERSION 0.1.4 LANGUAGES CXX)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

find_package(Qt5 COMPONENTS Core Widgets REQUIRED)
set(CMAKE_AUTOUIC_SEARCH_PATHS ${CMAKE_CURRENT_SOURCE_DIR}/ui)

add_executable(qimg2pdf
    ui/mainwindow.ui
    ui/imageview.ui
    src/main.cpp
    src/mainwindow.cpp
    src/imageview.cpp
    include/mainwindow.h
    include/imageview.h
    res/resources.qrc
)

target_include_directories(qimg2pdf PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)

target_link_libraries(qimg2pdf PUBLIC Qt5::Core Qt5::Widgets)

set_target_properties(qimg2pdf PROPERTIES
        CXX_STANDARD 11
        CXX_STANDARD_REQUIRED YES
        CXX_EXTENSIONS NO)

install(TARGETS qimg2pdf
        RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}")

install(FILES res/launcher.desktop
        DESTINATION "${CMAKE_INSTALL_DATAROOTDIR}/applications"
        RENAME qimg2pdf.desktop)

install(FILES res/app-icon.png
        DESTINATION "${CMAKE_INSTALL_DATAROOTDIR}/pixmaps"
        RENAME qimg2pdf.png)

install(FILES res/app-icon.svg
        DESTINATION "${CMAKE_INSTALL_DATAROOTDIR}/pixmaps"
        RENAME qimg2pdf.svg)
